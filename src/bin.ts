import fs from 'fs'
import gg from './index'

fs.createReadStream('./assets/values.csv')
  .pipe(new gg())
  .on('data', (x: any) => console.log(x))
