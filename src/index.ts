import { Transform } from 'stream'
import deepmerge from 'deepmerge'
import util from 'util'


const defaults: any = {
  skip_flags: ["#"],
  type_declaration: true,
}


export default class CsvToJs extends Transform {
  private header: string[] = []
  private content: string[][] = []
  private result: any

  constructor(private opts: any = {}) {
    super({ objectMode: true, highWaterMark: 16 })
  }

  cell_looper<T>(row: string, callback: (cell: string) => T): T[]{
    return row.split(/\,/g).map((cell: string) => callback(cell))
  }
  
  row_looper<T>(data: string, callback: (cell: string) => T): any {
    return row.split(/\r?\n/g).map((cell: string) => callback(cell))
  }

  cell_data_allocator(data: string|number|boolean, header_index: number) {
    this.header[header_index]
  }
  
  var_generator(variable: string|string[]|undefined, i: number = 0): any {
    if (Array.isArray(variable)) {
      if (variable.length > 2)
        return {[variable[0]]: this.var_generator(variable.splice(1), i+1)}
      else
        return {[variable[0]]: this.var_generator(variable[0], i+1)}
    } else if (typeof variable == 'string') {
      console.log(i)
      return {[variable]: /^\d+$/.test(variable) ? undefined : 'string'}
    }
  }

  generate_object_props(data_str: string[]) {
    const header: string[] = this.cell_looper(
      data_str[0],
      (th: string): any => {
        const variable = /\./g.test(th) ? th.split(/\./g) : th
        return this.var_generator(variable)
      }
    )
    this.header = header
  }

  private row_writter(data_str: string): string[] { return data_str.split(/\,/g) }

  private keep_line(row: string): boolean {
    if (row.length > 0) {
      for (let i in defaults.skip_flags) {
        if (row.indexOf(defaults.skip_flags[i]) == 0) {
          return false
        }
      }
      return true
    }
    return false
  }

  private declare_content(data_str: string[]) {
    this.content = data_str.slice(1).map(this.row_writter)
  }

  private convert_to_object() {
    this.result = this.content.map(x => deepmerge.all(this.row_to_object(x)))
  }

  private row_to_object(row: string[]) {
    return this.header
      .map((key: string, index: number) => {
        return this.arr_to_obj(key.split(/\./g), row[index])
      }, {})
  }

  private arr_to_obj(arr: string[], val: any): any {
    return arr.length > 1
      ? {[arr[0]]: this.arr_to_obj(arr.splice(1), val)}
      : {[arr[0]]: val}
  }

  _transform (data: any, enc: any, cb: any) {
    const data_str = data.toString('utf-8')
      .split(/\r?\n/g)
      .filter(this.keep_line)

    this.generate_object_props(data_str)
    console.log(util.inspect(this.header, false, null, true))
    // this.declare_content(data_str)
    // this.convert_to_object()

    this.push(this.result)
  }
}
